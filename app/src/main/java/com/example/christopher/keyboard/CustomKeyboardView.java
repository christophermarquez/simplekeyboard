package com.example.christopher.keyboard;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ShapeDrawable;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.KeyboardView;
import android.util.AttributeSet;
import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by christopher on 1/03/17.
 */

public class CustomKeyboardView extends KeyboardView {

    private AssetManager manager;
    private Settings settings;
    private final List<Character> focusKeys = initFocusKeys();
    private boolean caps = false;
    private Rect keySpace;

    private static List<Character> initFocusKeys() {
        List<Character> vowels = Arrays.asList('a', 'e', 'i', 'o', 'u');
        List<Character> operator = Arrays.asList('+', '–', '×', '÷', '=');
        List<Character> specialKeys = Arrays.asList('B', 'E', 'V', 'L');
        ArrayList<Character> allSymbols = new ArrayList<>();
        allSymbols.addAll(vowels);
        allSymbols.addAll(operator);
        allSymbols.addAll(specialKeys);
        return allSymbols;
    }

    public CustomKeyboardView(Context context, AttributeSet attrSet) {
        super(context, attrSet);
    }

    public CustomKeyboardView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void addAssetManager(AssetManager manager) {
        this.manager = manager;
    }

    public void caps(boolean caps) {
        this.caps = caps;
        this.refreshDrawableState();
    }

    public void updateSettings(Settings settings) {
        this.settings = settings;
        this.refreshDrawableState();
    }

    private Drawable drawKeyFront(Keyboard.Key key, int offset) {
        int keyColor;
        if (focusKeys.contains(key.label.charAt(0)) && settings.areVowelsDistinct()) {
            keyColor = getResources().getColor(R.color.vowels);
        } else {
            keyColor = settings.getKeyPrimaryColor();
        }
        if (key.label.toString().equals("Mayús") && this.caps) {
            keyColor = getResources().getColor(R.color.capsLock);
        }
        Drawable keyFront = new ShapeDrawable();
        keyFront.setBounds(key.x + offset, key.y + offset, key.x + key.width - offset, key.y + key.height - offset);
        this.keySpace = keyFront.getBounds();
        keyFront.setColorFilter(keyColor, PorterDuff.Mode.SRC_OVER);
        return keyFront;
    }

    private Drawable drawKeyBack(Keyboard.Key key) {
        Drawable keyBack = new ShapeDrawable();
        keyBack.setBounds(key.x, key.y, key.x + key.width, key.y + key.height);
        keyBack.setColorFilter(settings.getKeySecondaryColor(), PorterDuff.Mode.SRC_OVER);
        return keyBack;
    }

    private void drawKeyText(Keyboard.Key key, Canvas canvas) {
        String keyLabel;
        int keycode = key.codes[0];
        int codeType = (keycode - 255) / key.label.charAt(0);
        if (keycode > 255 && codeType == 1) { //Two letter code
            keyLabel = settings.getSelectedConsonant() + key.label.toString();
        } else if (keycode > 255 && codeType == 2) {
            keyLabel = key.label.toString() + settings.getSelectedConsonant();
        } else {
            keyLabel = key.label.toString();
        }
        if (this.caps) keyLabel = keyLabel.toUpperCase();
        Paint paint = settings.getFontPaint(manager);
        Rect textBounds = new Rect();
        paint.getTextBounds(keyLabel, 0, keyLabel.length(), textBounds);
        int keyTextX = keySpace.centerX() - textBounds.width() / 2;
        int keyTextY = keySpace.centerY() + textBounds.height() / 2;
        canvas.drawText(keyLabel, keyTextX, keyTextY, paint);
    }

    @Override
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        for (Keyboard.Key key : this.getKeyboard().getKeys()) {
            drawKeyBack(key).draw(canvas);
            drawKeyFront(key, 2).draw(canvas);
            drawKeyText(key, canvas);
        }
    }
}
