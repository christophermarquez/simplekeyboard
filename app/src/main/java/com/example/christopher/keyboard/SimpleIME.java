package com.example.christopher.keyboard;

import android.content.SharedPreferences;
import android.inputmethodservice.InputMethodService;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.KeyboardView;
import android.media.AudioManager;
import android.preference.PreferenceManager;
import android.speech.tts.TextToSpeech;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputConnection;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Created by christopher on 1/02/17.
 */

enum KeyboardViewCode {
    MainView,
    NoNumeric,
    NumericView,
    TwoLetterView,
    ConsonantsOne,
    ConsonantsTwo,
    ConsonantsThree,
    Vowels
}

public class SimpleIME extends InputMethodService implements KeyboardView.OnKeyboardActionListener, SharedPreferences.OnSharedPreferenceChangeListener{

    private Map<KeyboardViewCode, Keyboard> keyboardSet;
    private Map<Character, String> ttsKeys;
    private CustomKeyboardView keyboardView;
    private boolean caps = false;
    private int currentViewCode = 0;
    private Settings settings;
    private KeyboardViewCode viewCode = KeyboardViewCode.MainView;
    private TextToSpeech tts;

    public View onCreateInputView() {
        keyboardView = (CustomKeyboardView) getLayoutInflater().inflate(R.layout.keyboard, null);
        keyboardView.addAssetManager(getAssets());

        keyboardSet = new HashMap<>();
        keyboardSet.put(KeyboardViewCode.MainView, new Keyboard(this, R.xml.qwerty));
        keyboardSet.put(KeyboardViewCode.NumericView, new Keyboard(this, R.xml.numeric));
        keyboardSet.put(KeyboardViewCode.TwoLetterView, new Keyboard(this, R.xml.twoletter));
        keyboardSet.put(KeyboardViewCode.NoNumeric, new Keyboard(this, R.xml.nonumeric));
        keyboardSet.put(KeyboardViewCode.ConsonantsOne, new Keyboard(this, R.xml.consonants_block1));
        keyboardSet.put(KeyboardViewCode.ConsonantsTwo, new Keyboard(this, R.xml.consonants_block2));
        keyboardSet.put(KeyboardViewCode.ConsonantsThree, new Keyboard(this, R.xml.consonants_block3));
        keyboardSet.put(KeyboardViewCode.Vowels, new Keyboard(this, R.xml.vowels));

        ttsKeys = new HashMap<>();
        ttsKeys.put('y', "i griega");
        ttsKeys.put('k', "ka");
        ttsKeys.put('q', "ku");
        ttsKeys.put('w', "doble u");

        keyboardView.setKeyboard(keyboardSet.get(KeyboardViewCode.MainView));
        keyboardView.setOnKeyboardActionListener(this);

        keyboardView.updateSettings(settings = new Settings(PreferenceManager.getDefaultSharedPreferences(this)));
        PreferenceManager.getDefaultSharedPreferences(this).registerOnSharedPreferenceChangeListener(this);

        tts = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                tts.setLanguage(new Locale("es", "ES"));
            }
        });
        return this.keyboardView;
    }

    private void playClick(int keyCode) {
        AudioManager am = (AudioManager) getSystemService(AUDIO_SERVICE);
        switch(keyCode){
            case 32:
                am.playSoundEffect(AudioManager.FX_KEYPRESS_SPACEBAR);
                break;
            case Keyboard.KEYCODE_DONE:
            case 10:
                am.playSoundEffect(AudioManager.FX_KEYPRESS_RETURN);
                break;
            case Keyboard.KEYCODE_DELETE:
                am.playSoundEffect(AudioManager.FX_KEYPRESS_DELETE);
                break;
            default: am.playSoundEffect(AudioManager.FX_KEY_CLICK);
        }
    }

    @Override
    public void onPress(int primaryCode) {
    }

    @Override
    public void onRelease(int primaryCode) {

    }

    @Override
    public void onKey(int primaryCode, int[] keyCodes) {
        InputConnection inputConnection = this.getCurrentInputConnection();
        this.playClick(primaryCode);
        if (primaryCode > 255) {
            int order = primaryCode > 400 ? 2 : 1;
            char vowel = (char) (primaryCode % 255);
            String syllable = "";
            if (order == 1) { //Consonant first
                syllable += settings.getSelectedConsonant() + Character.toString(vowel);
            } else {
                vowel = (char) (vowel / 2);
                syllable += Character.toString(vowel) + settings.getSelectedConsonant();
            }
            inputConnection.commitText(syllable, 2);
            if (settings.isTtsEnabled()) tts.speak(syllable, TextToSpeech.QUEUE_FLUSH, null);
        } else {
            switch(primaryCode) {
                case -30:
                    inputConnection.commitText("ll", 2);
                    break;
                case Keyboard.KEYCODE_DELETE :
                    inputConnection.deleteSurroundingText(1, 0);
                    break;
                case Keyboard.KEYCODE_SHIFT:
                    caps = !caps;
                    keyboardSet.get(KeyboardViewCode.MainView).setShifted(caps);
                    keyboardView.invalidateAllKeys();
                    keyboardView.caps(this.caps);
                    break;
                case Keyboard.KEYCODE_DONE:
                    inputConnection.sendKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_ENTER));
                    break;
                case -20: //Change view
                    int value = ++this.currentViewCode;
                    int size = KeyboardViewCode.values().length;
                    this.viewCode = KeyboardViewCode.values()[value % size];
                    this.changeView(viewCode);
                    break;
                default:
                    char code = (char) primaryCode;
                    if(Character.isLetter(code) && caps){
                        code = Character.toUpperCase(code);
                    }
                    inputConnection.commitText(String.valueOf(code), 1);
                    if (!settings.isTtsEnabled()) break;
                    if (ttsKeys.containsKey(code)) {
                        tts.speak(ttsKeys.get(code), TextToSpeech.QUEUE_FLUSH, null);
                    } else {
                        tts.speak(code + "", TextToSpeech.QUEUE_FLUSH, null);
                    }
            }
        }
    }

    @Override
    public void onText(CharSequence text) {

    }

    @Override
    public void swipeLeft() {

    }

    @Override
    public void swipeRight() {

    }

    @Override
    public void swipeDown() {

    }

    @Override
    public void swipeUp() {

    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        settings = new Settings(sharedPreferences);
        keyboardView.updateSettings(settings);
    }

    private void changeView(KeyboardViewCode view) {
        keyboardView.setKeyboard(keyboardSet.get(view));
        keyboardView.refreshDrawableState();
    }
}
