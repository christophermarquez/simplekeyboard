package com.example.christopher.keyboard;

import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;

/**
 * Created by christopher on 23/02/17.
 */

public class Settings {
    private String keyPrimaryColor;
    private String keySecondaryColor;
    private String keyTextColor;
    private String fontSize;
    private boolean distinctVowels;
    private String selectedConsonant;
    private boolean ttsEnabled;

    public Settings(SharedPreferences preferences) {
        this.keyPrimaryColor = preferences.getString("pref_keyboard_KeyPrimColor", "#ffffff");
        this.keySecondaryColor = preferences.getString("pref_keyboard_KeySecColor", "#000000");
        this.keyTextColor = preferences.getString("pref_keyboard_keyTextColor", "#000000");
        this.fontSize = preferences.getString("pref_keyboard_keyFontSize", "36");
        this.distinctVowels = preferences.getBoolean("pref_keyboard_distinctVowels", true);
        this.selectedConsonant = preferences.getString("pref_keyboard_selectedConsonant", "m");
        this.ttsEnabled = preferences.getBoolean("pref_keyboard_text_to_speech", true);
    }

    public int getKeyPrimaryColor() {
        return Color.parseColor(keyPrimaryColor);
    }

    public int getKeySecondaryColor() {
        return Color.parseColor(keySecondaryColor);
    }
    public boolean areVowelsDistinct() {
        return distinctVowels;
    }
    public String getSelectedConsonant() {
        return selectedConsonant;
    }
    public boolean isTtsEnabled() {
        return ttsEnabled;
    }

    public Paint getFontPaint(AssetManager manager) {
        Typeface typeface = Typeface.createFromAsset(manager, "century.ttf");
        Paint paint = new Paint();
        paint.setTextSize(Integer.parseInt(fontSize));
        paint.setColor(Color.parseColor(keyTextColor));
        paint.setTypeface(typeface);
        return paint;
    }
}
