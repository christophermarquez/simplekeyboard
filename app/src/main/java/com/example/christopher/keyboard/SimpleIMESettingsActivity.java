package com.example.christopher.keyboard;

import android.app.Activity;
import android.os.Bundle;

/**
 * Created by christopher on 23/02/17.
 */

public class SimpleIMESettingsActivity extends Activity {

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getFragmentManager().beginTransaction()
        .replace(android.R.id.content, new SimpleIMESettingsFragment())
        .commit();
    }
}
